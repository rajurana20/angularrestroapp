import { Component, OnInit } from '@angular/core';
import {RestroServiceService} from '../restro-service.service'
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  dbUser:any={}
  isLoginSuccess:boolean=false;

  constructor(private restroService : RestroServiceService) { }
  alert:boolean=false;
  ngOnInit(): void {
  }

  loginForm = new FormGroup({
    password: new FormControl(''),
    email: new FormControl('')
  });
  onLogin()
  {
    this.isLoginSuccess = this.restroService.login(this.loginForm.value);
    this.alert=true;
  }
  
  closeAlert()
  {
    this.alert=false;
  }

}
