import { Component, OnInit } from '@angular/core';
import {RestroServiceService} from '../restro-service.service'

@Component({
  selector: 'app-list-restro',
  templateUrl: './list-restro.component.html',
  styleUrls: ['./list-restro.component.css']
})
export class ListRestroComponent implements OnInit {
  restroList:any=[];
  constructor(private restroService:RestroServiceService) { }

  ngOnInit(): void {
    this.restroService.getData().subscribe(
      (result)=>{
        this.restroList=result
      }
    )
  }

  deleteRestro(id){
    this.restroList.splice(id-1, 1);
      this.restroService.deleteData(id).subscribe(
      (result)=>{console.warn(result)}
    )
  }

}
