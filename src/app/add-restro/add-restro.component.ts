import { Component, OnInit } from '@angular/core';
import {RestroServiceService} from '../restro-service.service'
import { FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-add-restro',
  templateUrl: './add-restro.component.html',
  styleUrls: ['./add-restro.component.css']
})
export class AddRestroComponent implements OnInit {

  constructor(private restroService : RestroServiceService) { }
  alert:boolean=false;
  ngOnInit(): void {
  }

  restroAddForm = new FormGroup({
    name: new FormControl('',),
    address: new FormControl(''),
    email: new FormControl('')
  });

  onSave()
  {
    this.restroService.saveData(this.restroAddForm.value).subscribe(
      (result)=>{
        this.alert=true;
        this.restroAddForm.reset({});
      }
    )
    
  }

  closeAlert()
  {
    this.alert=false;
  }

}
