import { Component, OnInit } from '@angular/core';
import {RestroServiceService} from '../restro-service.service'
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private restroService : RestroServiceService) { }
  alert:boolean=false;
  ngOnInit(): void {
  }

  registrationForm = new FormGroup({
    name: new FormControl('',),
    password: new FormControl(''),
    email: new FormControl('')
  });

  onSave()
  {
    this.restroService.register(this.registrationForm.value).subscribe(
      (result)=>{
        this.alert=true;
        console.log(result)
        this.registrationForm.reset({});
      }
    )
    
  }

  closeAlert()
  {
    this.alert=false;
  }

}
