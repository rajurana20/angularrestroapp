import { Component } from '@angular/core';

import {RestroServiceService} from './restro-service.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'restro';
  restroList:any;
  constructor(private restroService:RestroServiceService){}
  ngOnInit()
  {
  }
  
}
