import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {ActivatedRoute} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RestroServiceService {
  url='http://localhost:3000/resturants';
  userUrl='http://localhost:3000/users';
  tempUser:any=[];

  constructor(private http:HttpClient){}

  getData()
  {
    return this.http.get(this.url);
  }
  saveData(data)
  {
    return this.http.post(this.url,data);
  }
  deleteData(id)
  {
    return this.http.delete(this.url+'/'+id);
  }
  getCurrentRestro(id)
  {
    return this.http.get(this.url+'/'+id);
  }
  updateData(id,data)
  {
    return this.http.put(this.url+'/'+id, data);
  }
  register(data)
  {
    return this.http.post(this.userUrl,data);
  }
  findUser(email)
  {
    return this.http.get(this.userUrl+"?email="+email);
  }
  login(data)
  { 
    debugger;
    this.findUser(data.email).subscribe(
      (result)=> {this.tempUser=result}
      );
    console.log("provided user ", data);
    console.log("database user",this.tempUser);
    this.tempUser.forEach(element => {
      if(element.password==data.password)
      {
        return true;
      }
    }); 
    return false;
  }
}
