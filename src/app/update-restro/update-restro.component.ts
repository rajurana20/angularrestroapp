import { Component, OnInit } from '@angular/core';
import {RestroServiceService} from '../restro-service.service';
import {FormGroup, FormControl} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-update-restro',
  templateUrl: './update-restro.component.html',
  styleUrls: ['./update-restro.component.css']
})
export class UpdateRestroComponent implements OnInit {

  constructor(private restroService : RestroServiceService, private router:ActivatedRoute) { }
  alert:boolean=false;
  restroUpdateForm = new FormGroup({
    name: new FormControl('',),
    address: new FormControl(''),
    email: new FormControl('')
  });

  ngOnInit()
  {
    console.log(this.router.snapshot.params.id);
    this.getCurrentRestro(this.router.snapshot.params.id);
    
  }


  getCurrentRestro(id)
  {
    this.restroService.getCurrentRestro(id).subscribe(
      (result)=> {
        console.log(result);
        this.restroUpdateForm = new FormGroup({
          name: new FormControl(result['name']),
          address: new FormControl(result['address']),
          email: new FormControl(result['email'])
        });
      }
    )
  }
  onSave()
  {
    this.restroService.updateData(this.router.snapshot.params.id,this.restroUpdateForm.value).subscribe(
      (result)=>{
        this.alert=true;
        this.restroUpdateForm.reset({});
      }
    )
    
  }

  closeAlert()
  {
    this.alert=false;
  }

}
